package com.moravskiyandriy;

import com.moravskiyandriy.commands.Menu;
import com.moravskiyandriy.texthandler.TextHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Locale locale = new Locale("en");
        ResourceBundle bundle = ResourceBundle.getBundle("Menu", locale);
        TextHandler textHandler = new TextHandler();
        Scanner scanner = new Scanner(System.in);

        logger.info("\nChose language: 1-EN, 2-DE, 3-CN");
        int language = scanner.nextInt();
        if (language == 2) {
            locale = new Locale("de");
            bundle = ResourceBundle.getBundle("Menu", locale);
        }
        if (language == 3) {
            locale = new Locale("zh");
            bundle = ResourceBundle.getBundle("Menu", locale);
        }

        Menu menu = new Menu(textHandler, bundle);
        while (true) {
            logger.info("\nMenu: ");
            menu.printMenu();
            logger.info("Your choise:");
            String choise = scanner.nextLine();
            if (choise.isEmpty()) {
                choise = scanner.nextLine();
            }
            if (choise.equals("q")) {
                break;
            }
            if (choise.equals("de")) {
                locale=new Locale("de");
                bundle=ResourceBundle.getBundle("Menu", locale);
                menu.setMenu(bundle);
                continue;
            }
            if (choise.equals("en")) {
                locale=new Locale("en");
                bundle=ResourceBundle.getBundle("Menu", locale);
                menu.setMenu(bundle);
                continue;
            }
            if (choise.equals("zh")) {
                locale=new Locale("zh");
                bundle=ResourceBundle.getBundle("Menu", locale);
                menu.setMenu(bundle);
                continue;
            }
            menu.chose(Integer.valueOf(choise));
        }
    }
}
