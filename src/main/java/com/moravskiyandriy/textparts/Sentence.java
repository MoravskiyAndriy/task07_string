package com.moravskiyandriy.textparts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    private static final Logger logger = LogManager.getLogger(Sentence.class);
    private List<SentencePart> sentence;

    public Sentence() {
        sentence = new ArrayList<>();
    }

    public Sentence(Sentence s) {
        sentence = new ArrayList<>();
        s.sentence.forEach(sp -> sentence.add(sp.copy()));
    }

    public void add(SentencePart sp) {
        sentence.add(sp);
    }

    public List<SentencePart> getSentence() {
        return sentence;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        sentence.forEach(p -> {
            if ((p instanceof Word)) {
                stringBuilder.append(p.toString()).append(" ");
            } else if (p instanceof PunctuationSign) {
                try {
                    stringBuilder.setLength(stringBuilder.length() - 1);
                } catch (StringIndexOutOfBoundsException e) {
                    logger.warn("StringIndexOutOfBoundsException");
                }
                stringBuilder.append(p.toString());
            } else {
                stringBuilder.append(p.toString());
            }
        });
        return stringBuilder.toString();
    }

    public void deleteWord(Word a) {
        Word aArg = new Word(a.toString());
        List<SentencePart> s1 = new ArrayList<>();
        for (SentencePart sp : sentence) {
            if (sp.toString().equals(aArg.toString())) {
                continue;
            }
            s1.add(sp.copy());
        }
        this.sentence = s1;
    }

    public void replace(final Word a, final Word b) {
        boolean flagForA = true;
        boolean flagForB = true;
        Word aArg = new Word(a.toString());
        Word bArg = new Word(b.toString());

        for (SentencePart sp : sentence) {
            if (sp.toString().equals(aArg.toString()) && flagForA) {
                sp.set(bArg.toString());
                flagForA = false;
                continue;
            }
            if (sp.toString().equals(bArg.toString()) && flagForB) {
                sp.set(aArg.toString());
                flagForB = false;
            }
            if (!flagForA && !flagForB) {
                return;
            }
        }
    }
}
