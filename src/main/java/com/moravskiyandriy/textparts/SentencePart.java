package com.moravskiyandriy.textparts;

public abstract class SentencePart {
    SentencePart() {
    }

    @Override
    public abstract String toString();

    public abstract void set(String str);

    public abstract SentencePart copy();
}
