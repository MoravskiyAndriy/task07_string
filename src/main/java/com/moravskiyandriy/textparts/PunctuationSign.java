package com.moravskiyandriy.textparts;

public class PunctuationSign extends SentencePart {
    private String sign;

    public PunctuationSign(Character str) {
        sign = str.toString();
    }

    private PunctuationSign(String str) {
        sign = str;
    }

    public void set(String str) {
    }

    @Override
    public SentencePart copy() {
        return new PunctuationSign(sign);
    }

    @Override
    public String toString() {
        return sign + " ";
    }
}
