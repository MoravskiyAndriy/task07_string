package com.moravskiyandriy.textparts;

public class Space extends SentencePart {

    public Space() {
    }

    public void set(String str) {
    }

    @Override
    public SentencePart copy() {
        return new Space();
    }

    @Override
    public String toString() {
        return " ";
    }
}
