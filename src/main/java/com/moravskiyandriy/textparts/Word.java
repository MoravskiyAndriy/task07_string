package com.moravskiyandriy.textparts;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word extends SentencePart implements Comparable<Word> {

    private String word;

    public Word(String str) {
        word = str;
    }

    public int len() {
        return word.length();
    }

    public void set(String str) {
        this.word = str;
    }

    @Override
    public SentencePart copy() {
        return new Word(word);
    }

    @Override
    public int compareTo(Word w) {
        return word.compareTo(w.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.isNull(o)) {
            return false;
        }
        if (!(o instanceof Word)) {
            return false;
        }
        return word.equals(o.toString());
    }

    public BigDecimal getVowelPercentage() {
        int counter = 0;
        String pattern = "(?i)[aeiou]";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(word);
        while (m.find()) {
            counter++;
        }
        if (counter != 0) {
            return new BigDecimal(word.length() / (double) counter);
        }
        return new BigDecimal(0);
    }

    public int getFirtConsonantCode() {
        String consonant = word.toLowerCase().replaceAll("[AEIOUaeiou]", "");
        if (!consonant.isEmpty()) {
            return (int) consonant.toCharArray()[0];
        }
        return 0;
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public String toString() {
        return word;
    }
}
