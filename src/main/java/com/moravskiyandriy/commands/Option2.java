package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option2 implements Command {
    private TextHandler textHandler;

    Option2(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showSentencesByWordsQuantity();
    }
}
