package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option7 implements Command {
    private TextHandler textHandler;

    Option7(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showWordsByVowelsPercentage();
    }
}