package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option15 implements Command {
    private TextHandler textHandler;

    Option15(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showProgressDeletedLetterText();
    }
}
