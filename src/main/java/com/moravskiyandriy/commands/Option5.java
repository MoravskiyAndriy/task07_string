package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option5 implements Command {
    private TextHandler textHandler;

    Option5(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showFirstVowelToLongestWord();
    }
}
