package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option14 implements Command {
    private TextHandler textHandler;

    Option14(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showLongestPalindrome();
    }
}
