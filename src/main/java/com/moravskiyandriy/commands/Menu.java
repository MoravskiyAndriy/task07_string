package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.Constants;
import com.moravskiyandriy.texthandler.TextHandler;

import java.util.*;

public class Menu {
    private CommandSwitcher commandSwitcher;
    private List<String> menu = new ArrayList<>();

    ResourceBundle bundle;

    public Menu(TextHandler textHandler, ResourceBundle bundle) {
        this.bundle=bundle;
        commandSwitcher = new CommandSwitcher();
        commandSwitcher.addCommand(new Option1(textHandler));
        commandSwitcher.addCommand(new Option2(textHandler));
        commandSwitcher.addCommand(new Option3(textHandler));
        commandSwitcher.addCommand(new Option4(textHandler));
        commandSwitcher.addCommand(new Option5(textHandler));
        commandSwitcher.addCommand(new Option6(textHandler));
        commandSwitcher.addCommand(new Option7(textHandler));
        commandSwitcher.addCommand(new Option8(textHandler));
        commandSwitcher.addCommand(new Option9(textHandler));
        commandSwitcher.addCommand(new Option10(textHandler));
        commandSwitcher.addCommand(new Option11(textHandler));
        commandSwitcher.addCommand(new Option12(textHandler));
        commandSwitcher.addCommand(new Option13(textHandler));
        commandSwitcher.addCommand(new Option14(textHandler));
        commandSwitcher.addCommand(new Option15(textHandler));
        commandSwitcher.addCommand(new Option16(textHandler));
        setMenu(bundle);
    }

    public void chose(final Integer number) {
        commandSwitcher.execute(number);
    }

    public void printMenu() {
        for (String s : menu) {
            System.out.println(s);
        }
    }

    public void setMenu(ResourceBundle bundle) {
        menu = new ArrayList<>();
        for(int i=0;i<Constants.COMMAND_NUMBER;i++){
            menu.add((bundle.getString(String.valueOf(i))));
        }
    }

    public void quiqTest() {
        TextHandler tr = new TextHandler();
        tr.showSentencesNumberWithSameWords();
        tr.showSentencesByWordsQuantity();
        tr.showUniqueFirstSentenceWord();
        tr.showQuestionMarkFixedLenWords(3);
        tr.showFirstVowelToLongestWord();
        tr.showWordsInAlphabeticalOrder();
        tr.showWordsByVowelsPercentage();
        tr.showFirstVowelSortedByConsonant();
        tr.showSortedByLetterQuantity('a');
        tr.showWordsStatistics();
        tr.deleteSubstring("a", "a");
        tr.showTextConsonantFirstDeleteWithLength(4);
        tr.showSortedBySymbolQuantity('a');
        tr.showLongestPalindrome();
        tr.showProgressDeletedLetterText();
        tr.showWordsReplacedBySubstring(5, 4, "@@@@@@@@@@@@@@");
    }
}
