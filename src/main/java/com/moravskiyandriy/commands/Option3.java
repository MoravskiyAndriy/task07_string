package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option3 implements Command {
    private TextHandler textHandler;

    Option3(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showUniqueFirstSentenceWord();
    }
}
