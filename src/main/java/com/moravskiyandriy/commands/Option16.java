package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Option16 implements Command {
    private static final Logger logger = LogManager.getLogger(Option16.class);
    private TextHandler textHandler;

    Option16(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        Scanner input = new Scanner(System.in);
        logger.info("Input sentence number: ");
        int val1 = input.nextInt();
        logger.info("Input word length: ");
        int val2 = input.nextInt();
        logger.info("Input substring: ");
        String val3 = input.next();
        textHandler.showWordsReplacedBySubstring(val1, val2, val3);
    }
}
