package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option1 implements Command {
    private TextHandler textHandler;

    Option1(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showSentencesNumberWithSameWords();
    }
}
