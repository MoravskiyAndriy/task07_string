package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option6 implements Command {
    private TextHandler textHandler;

    Option6(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showWordsInAlphabeticalOrder();
    }
}