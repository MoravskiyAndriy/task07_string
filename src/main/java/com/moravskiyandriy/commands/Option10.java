package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option10 implements Command {
    private TextHandler textHandler;

    Option10(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showWordsStatistics();
    }
}