package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Option9 implements Command {
    private static final Logger logger = LogManager.getLogger(Option9.class);
    private TextHandler textHandler;

    Option9(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        Scanner input = new Scanner(System.in);
        logger.info("Input character: ");
        char key = input.next().charAt(0);
        textHandler.showSortedByLetterQuantity(key);
    }
}
