package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Option4 implements Command {
    private static final Logger logger = LogManager.getLogger(Option4.class);
    private TextHandler textHandler;

    Option4(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        Scanner input = new Scanner(System.in);
        logger.info("Input length: ");
        int key = input.nextInt();
        textHandler.showQuestionMarkFixedLenWords(key);
    }
}
