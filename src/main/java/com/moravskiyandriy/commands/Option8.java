package com.moravskiyandriy.commands;

import com.moravskiyandriy.texthandler.TextHandler;

public class Option8 implements Command {
    private TextHandler textHandler;

    Option8(final TextHandler textHandler) {
        this.textHandler = textHandler;
    }

    public void execute() {
        textHandler.showFirstVowelSortedByConsonant();
    }
}