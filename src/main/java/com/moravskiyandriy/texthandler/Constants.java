package com.moravskiyandriy.texthandler;

import java.util.Arrays;
import java.util.List;

public class Constants {
    private Constants(){}

    final static List<Character> ALL_P_SIGNS = Arrays.asList('.', '!', '?', ',', '/', '"', '(', ')', '{', '}', ':', ';', '&', '$', '—', '-', '“', '”', '"');
    final static List<Character> TERMINAL_P_SIGNS = Arrays.asList('.', '!', '?');
    final static String FILE_NAME = "text.txt";
    final static String TASK_FILE_NAME = "words.txt";
    public final static Integer COMMAND_NUMBER = 16;
}
