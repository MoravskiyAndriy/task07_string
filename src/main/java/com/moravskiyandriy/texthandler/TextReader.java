package com.moravskiyandriy.texthandler;

import com.moravskiyandriy.textparts.PunctuationSign;
import com.moravskiyandriy.textparts.Sentence;
import com.moravskiyandriy.textparts.Space;
import com.moravskiyandriy.textparts.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

class TextReader {
    private static final Logger logger = LogManager.getLogger(TextReader.class);
    private List<Sentence> allText;
    private List<Word> allWords;
    private List<String> taskWords;
    private String fileName;
    private String wordsFileName;

    void readText() {
        loadFiles();
        List<Character> allPSigns = Constants.ALL_P_SIGNS;
        List<Character> terminalPSigns = Constants.TERMINAL_P_SIGNS;
        readTaskWords();
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuilder wordBuilder = new StringBuilder();
            Sentence sentence = new Sentence();
            int code;
            while ((code = bufferedReader.read()) != -1) {
                if (terminalPSigns.contains((char) code)) {
                    if (!wordBuilder.toString().isEmpty()) {
                        addNewWord(sentence,wordBuilder);
                        wordBuilder = new StringBuilder();
                    }
                    sentence.add(new PunctuationSign((char) code));
                    allText.add(sentence);
                    sentence = new Sentence();
                } else if (allPSigns.contains((char) code)) {
                    if (!wordBuilder.toString().isEmpty()) {
                        addNewWord(sentence,wordBuilder);
                        wordBuilder = new StringBuilder();
                    }
                    sentence.add(new PunctuationSign((char) code));
                } else if ((char) code == ' ') {
                    if (!wordBuilder.toString().isEmpty()) {
                        addNewWord(sentence,wordBuilder);
                        wordBuilder = new StringBuilder();
                        sentence.add(new Space());
                    }
                } else {
                    wordBuilder.append((char) code);
                }
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            logger.info("Unable to open file '" + fileName + "'");
        } catch (IOException ex) {
            logger.info("Error reading file '" + fileName + "'");
        }
    }

    private void loadFiles(){
        Properties prop = new Properties();
        try (InputStream input = TextReader.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        fileName=Optional.ofNullable(prop.
                getProperty("FILE_NAME")).
                orElse(Constants.FILE_NAME);
        wordsFileName=Optional.ofNullable(prop.
                getProperty("TASK_FILE_NAME")).
                orElse(Constants.TASK_FILE_NAME);
        allWords = new ArrayList<>();
        allText = new ArrayList<>();
        taskWords = new ArrayList<>();
    }

    private void addNewWord(Sentence s, StringBuilder sb){
        s.add(new Word(sb.toString().replace(System.getProperty("line.separator"), "")));
        allWords.add(new Word(sb.toString().replace(System.getProperty("line.separator"), "")));
    }

    private void readTaskWords(){
        Scanner wordsReader = null;
        try {
            wordsReader = new Scanner(new File(wordsFileName));
        } catch (FileNotFoundException e) {
            logger.info("No file found.");
            e.printStackTrace();
        }
        if (Objects.nonNull(wordsReader)) {
            while (wordsReader.hasNextLine()) {
                Scanner temp = new Scanner(wordsReader.nextLine());
                while (temp.hasNext()) {
                    taskWords.add(temp.next());
                }
            }
        }
    }

    List<Sentence> getAllText() {
        return allText;
    }

    List<Word> getAllWords() {
        return allWords;
    }

    List<Word> getTaskWords() {
        return taskWords.stream()
                .map(Word::new)
                .collect(Collectors.toList());
    }
}
