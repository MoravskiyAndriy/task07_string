package com.moravskiyandriy.texthandler;

import com.moravskiyandriy.textparts.Sentence;
import com.moravskiyandriy.textparts.SentencePart;
import com.moravskiyandriy.textparts.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TextHandler {
    private static final Logger logger = LogManager.getLogger(TextHandler.class);
    private List<Sentence> allOriginalText;
    private List<Sentence> workingText;
    private List<Sentence> allFormattedText;
    private List<Word> allWords;
    private List<Word> allDistinctWords;
    private TextReader textReader;

    public TextHandler() {
        textReader = new TextReader();
        textReader.readText();
        allOriginalText = textReader.getAllText();
        workingText = new ArrayList<>();
        allDistinctWords = new ArrayList<>();
        List<Sentence> temp = new ArrayList<>();
        allOriginalText.forEach(s -> temp.add(new Sentence(s)));
        allWords = textReader.getAllWords();
        allDistinctWords = textReader
                .getAllWords()
                .stream()
                .map(p -> new Word(p.toString().toLowerCase()))
                .sorted()
                .distinct()
                .collect(Collectors.toList());
        allFormattedText = temp
                .stream()
                .peek(p -> p.
                        getSentence()
                        .forEach(e -> e.set(e.toString().toLowerCase())))
                .collect(Collectors.toList());
    }

    private String buildStringText(List<Sentence> list) {
        workingText = new ArrayList<>();
        list.forEach(s -> workingText.add(new Sentence(s)));
        StringBuilder textBuilder = new StringBuilder();
        if (!workingText.isEmpty()) {
            workingText.stream().map(Sentence::toString).forEach(textBuilder::append);
        }
        return textBuilder.toString();
    }

    public void showSentencesNumberWithSameWords() {
        Map<Word, Long> wordMap = new HashMap<>();
        allDistinctWords
                .forEach(p -> wordMap.put(p, allFormattedText
                        .stream()
                        .filter(s -> s.toString().contains(p.toString()))
                        .count()));
        logger.info(wordMap.values().stream().max(Comparator.comparing(a -> a)));
    }

    public void showSentencesByWordsQuantity() {
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        workingText.stream()
                .sorted((a, b) -> Long
                        .compare(a
                                        .getSentence()
                                        .stream()
                                        .filter(el -> el instanceof Word)
                                        .count(),
                                b
                                        .getSentence()
                                        .stream()
                                        .filter(el -> el instanceof Word)
                                        .count()))
                .forEach(logger::info);
    }

    public void showUniqueFirstSentenceWord() {
        Map<Word, Long> uniques = new HashMap<>();
        if (!allFormattedText.isEmpty()) {
            allFormattedText.get(0).getSentence().stream()
                    .filter(e -> e instanceof Word)
                    .forEach(wrd -> uniques.put((Word) wrd, allFormattedText.stream().filter(s -> s.toString().contains(wrd.toString())).count()));
        }
        if (!uniques.isEmpty()) {
            logger.info("Unique words: ");
            for (Map.Entry<Word, Long> entry : uniques.entrySet()) {
                if (entry.getValue().equals(1L)) {
                    logger.info(entry.getKey().toString());
                }
            }
        } else {
            logger.info("No unique words in first sentence.");
        }
    }

    public void showQuestionMarkFixedLenWords(int length) {
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        Set<Word> wordList = new HashSet<>();
        workingText.stream()
                .filter(s -> s.toString().contains("?"))
                .forEach(s -> s.getSentence()
                        .stream()
                        .filter(e -> e instanceof Word).filter(e -> ((Word) e).len() == length)
                        .forEach(p -> wordList.add((Word) p)));
        wordList.forEach(logger::info);
    }

    public void showFirstVowelToLongestWord() {
        List<Word> beginWithVowel = new ArrayList<>();
        List<Word> longestWord = new ArrayList<>();
        List<Sentence> alteredOriginal = new ArrayList<>();
        allOriginalText.forEach(s -> alteredOriginal.add(new Sentence(s)));
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        workingText
                .forEach(s -> beginWithVowel.add((Word) s
                        .getSentence()
                        .stream()
                        .filter(p -> p instanceof Word)
                        .filter(el -> Pattern.matches("(?i)^[aeiouy].*$", el.toString().toLowerCase()))
                        .findFirst()
                        .orElse(new Word(""))));
        workingText
                .forEach(s -> longestWord.add((Word) s
                        .getSentence()
                        .stream()
                        .filter(p -> p instanceof Word).min((a, b) -> Integer.compare(((Word) b).len(), ((Word) a).len()))
                        .orElse(new Word(""))));
        int iterator = 0;
        for (Sentence s : alteredOriginal) {
            if (beginWithVowel.get(iterator).toString().isEmpty()) {
                iterator++;
                continue;
            }
            s.replace(longestWord.get(iterator), beginWithVowel.get(iterator));
            iterator++;
        }
        alteredOriginal.forEach(logger::info);
    }

    public void showWordsInAlphabeticalOrder() {
        List<Word> wordList = allWords.stream()
                .sorted(Comparator.comparingInt(s -> s.toString()
                        .toLowerCase()
                        .toCharArray()[0]))
                .collect(Collectors.toList());
        String firstLetter = "a";
        if (!wordList.isEmpty()) {
            firstLetter = String.valueOf(wordList.get(0).toString().toCharArray()[0]);
        }
        StringBuilder sb = new StringBuilder();
        for (Word w : wordList) {
            final char c = w.toString().toLowerCase().toCharArray()[0];
            if (String.valueOf(c).equals(firstLetter)) {
                sb.append(w.toString()).append(" ");
            } else {
                sb.append("\n");
                firstLetter = String.valueOf(c);
                sb.append(w.toString());
            }
        }
        logger.info(sb.toString());
    }

    public void showWordsByVowelsPercentage() {
        allWords.stream().sorted(Comparator.comparing(Word::getVowelPercentage)).forEach(logger::info);
    }

    public void showFirstVowelSortedByConsonant() {
        allWords.stream()
                .filter(el -> Pattern.matches("(?i)^[aeiouy].*$", el.toString().toLowerCase()))
                .sorted(Comparator.comparing(Word::getFirtConsonantCode)).forEach(logger::info);
    }

    public void showSortedByLetterQuantity(char ch) {
        allWords.stream()
                .sorted(Comparator.comparing(s -> s
                        .toString()
                        .chars()
                        .filter(c -> c == ch)
                        .count())
                        .thenComparing(Object::toString))
                .forEach(logger::info);
    }

    public void showSortedBySymbolQuantity(char ch) {
        allWords.stream()
                .sorted(Comparator.comparing(s -> s
                        .toString()
                        .chars()
                        .filter(c -> c == ch)
                        .count()).reversed()
                        .thenComparing(Object::toString))
                .forEach(logger::info);
    }

    public void showTextConsonantFirstDeleteWithLength(int len) {
        List<Word> toDelete = new ArrayList<>();
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        allWords.stream()
                .filter(el -> Pattern.matches("(?i)[a-z&&[^aeiou]].*$", el.toString().toLowerCase()))
                .filter(el -> el.len() == len)
                .forEach(toDelete::add);
        toDelete.forEach(s -> workingText.forEach(v -> v.deleteWord(s)));
        workingText.forEach(logger::info);
    }

    public void deleteSubstring(String begin, String end) {
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        List<String> resultedList = workingText.stream()
                .map(Sentence::toString)
                .map(e -> (e.contains(begin) && e.contains(end)) ? e.substring(0,
                        e.indexOf(begin)) + e.substring(e.lastIndexOf(end) + 1) : e)
                .collect(Collectors.toList());
        resultedList.forEach(logger::info);
    }

    public void showLongestPalindrome() {
        workingText = new ArrayList<>();
        allFormattedText.forEach(s -> workingText.add(new Sentence(s)));
        String text = buildStringText(workingText);
        String longest = text.substring(0, 1);
        for (int i = 0; i < text.length(); i = i + 1) {
            String tmp = checkForEquality(text, i, i);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
            tmp = checkForEquality(text, i, i + 1);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
        }
        logger.info(longest);
    }

    private String checkForEquality(String s, int begin, int end) {
        while (begin >= 0 && end <= s.length() - 1 && s.charAt(begin) == s.charAt(end)) {
            begin--;
            end++;
        }
        return s.substring(begin + 1, end);
    }

    public void showProgressDeletedLetterText() {
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        for (Word w : allDistinctWords) {
            int counter = 0;
            String pattern = w.toString();
            for (Sentence s : workingText) {
                for (SentencePart sp : s.getSentence()) {
                    if (sp.toString().equalsIgnoreCase(pattern)) {
                        if (counter <= pattern.length()) {
                            sp.set(pattern.substring(counter));
                            counter++;
                        } else {
                            sp.set("");
                        }
                    }
                }
            }
        }
        workingText.stream().map(s -> s.toString().replaceAll("\\s{2,}", " ")).forEach(logger::info);
    }

    public void showWordsReplacedBySubstring(int sentenceNumber, int wordLength, String subSting) {
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        try {
            workingText.get(sentenceNumber - 1).getSentence()
                    .forEach(s -> {
                        if (s instanceof Word && ((Word) s).len() == wordLength) {
                            s.set(subSting);
                        }
                    });
        } catch (Exception e) {
            logger.info("Bad Number.");
            e.printStackTrace();
        }
        workingText.forEach(logger::info);
    }

    public void showWordsStatistics() {
        class WordEntity implements Comparable<WordEntity> {
            private Word word;
            private Collection<Integer> list;
            private int sum = 0;

            private WordEntity(Word word, Collection<Integer> list) {
                this.word = word;
                this.list = list;
                if (!list.isEmpty()) {
                    int counter = 0;
                    for (Integer i : list) {
                        counter += i;
                    }
                    sum = counter;
                }
            }

            @Override
            public int compareTo(WordEntity we) {
                return Integer.compare(sum, we.sum);
            }

            @Override
            public String toString() {
                return word.toString() + ":  " + list.stream().map(Object::toString).collect(Collectors.toList());
            }
        }
        Map<Word, Collection<Integer>> wordMap = new HashMap<>();
        workingText = new ArrayList<>();
        allOriginalText.forEach(s -> workingText.add(new Sentence(s)));
        textReader.getTaskWords().forEach(s -> wordMap.put(s, new ArrayList<>()));
        textReader.getTaskWords()
                .forEach(s -> workingText
                        .forEach(e -> wordMap.get(s).add(Math.toIntExact(e.getSentence().stream()
                                .filter(v -> v instanceof Word)
                                .filter(f -> f.toString().equalsIgnoreCase(s.toString())).count()))));
        List<WordEntity> statistics = new ArrayList<>();
        wordMap.forEach((K, V) -> statistics.add(new WordEntity(K, V)));
        statistics.stream().sorted().forEach(logger::info);
    }
}
