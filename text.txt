The                  base class Shape establishes the common interface to anything inherited 
from Shape—that is, all shapes can be drawn and erased. The derived classes 
override these definitions to provide unique behavior for each specific type of 
shape. 
RandomShapeGenerator is a kind of “factory” that produces a reference 
to a randomly selected Shape object each time you call its next( ) method. 
Note that the upcasting happens in the return statements, each of which 
takes a reference to a Circle, Square, or Triangle and sends it out of 
next( ) as the return type, Shape. So whenever you call next( ), you never 
get a chance to see what specific type it is, since you always get back a plain 
Shape reference. 
main( ) contains an array of Shape references filled through calls to 
RandomShapeGenerator.next( ). At this point you know you have 
Shapes, but you don’t know anything more specific than that (and neither 
does the compiler). However, when you step through this array and call 
draw( ) for each one, the correct type-specific behavior magically occurs, as 
you can see from the output when you run the program. 
The point of creating the shapes randomly is to drive home the 
understanding that the compiler can have no special knowledge that allows it 
to make the correct calls at compile time. All the calls to draw( ) must be 
made through dynamic binding. 
Exercise 2: (1) Add the (©Override annotation to the shapes example. 
Exercise 3: (1) Add a new method in the base class of Shapes.java that 
prints a message, but don’t override it in the derived classes. Explain what 
happens. Now override it in one of the derived classes but not the others, and 
see what happens. Finally, override it in all the derived classes. 
Exercise 4: (2) Add a new type of Shape to Shapes.java and verily in 
main( ) that polymorphism works for your new type as it does in the old 
types. 
Exercise 5: (1) Starting from Exercise 1, add a wheels( ) method in 
Cycle, which returns the number of wheels. Modify ride( ) to call wheels( ) 
and verify that polymorphism works. 
Extensibility 
Now let’s return to the musical instrument example? Because of 
polymorphism, you can add as many new types as you want to the system 
without changing the tune( ) method. In a well-designed OOP program, 
most or all of your methods will follow the model of tune( ) and 
communicate only with the base-class interface. Such a program is extensible 
because you can add new functionality by inheriting new data types from the 
common base class. The methods that manipulate the base-class interface 
will not need to be changed at all to accommodate the new classes. 
Consider what happens if you take the instrument example and add more 
methods in the base class and a number of new classes.